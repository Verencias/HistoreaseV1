const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
        
        
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors:{
                'blue-wh':'#1ec1d7',
                'pink-wh':'#fbadb7',
                'green-wh':'#2ed8c3',
                'dgreen-wh':'#014751',
                'red-wh':'#cc3262',
                'black-wh':'#333333',
            },
            gridTemplateColumns: {
                'custom-sidenav-layout': 'auto 1fr',
            },
            textShadow: {
                sm: '0 1px 2px var(--tw-shadow-color)',
                DEFAULT: '0 2px 4px var(--tw-shadow-color)',
                lg: '0 8px 16px var(--tw-shadow-color)',
              },
            
        },
        
    },

    plugins: [require('@tailwindcss/forms'),require('tailwindcss-textshadow')],
    
};
