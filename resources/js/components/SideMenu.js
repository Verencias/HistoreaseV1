import React, { useState } from 'react';
import { Link, Head } from '@inertiajs/inertia-react';
const SideMenu = () => {
  const [activePage, setActivePage] = useState("Dashboard");
  return (
    <div className="h-screen grid grid-cols-custom-sidenav-layout">
      <Sidenav activePage={activePage} setActivePage={setActivePage} />
      <Content activePage={activePage} />
    </div>
  );
}

const Sidenav = ({ activePage, setActivePage }) => (
  <div className="flex flex-col bg-green-900 text-green-50 px-6 py-4">
    <SidenavHeader />
    <SidenavMenu activePage={activePage} setActivePage={setActivePage} />
    <SidenavFooter />
  </div>
);

const SidenavHeader = () => (
  <div className="flex items-center ml-1 pb-8">
    <FireSvg />
    <a href="#home" className="text-xl font-bold pl-1 no-underline text-green-50 hover:text-green-100">bored.io</a>
  </div>
);

const SidenavMenu = ({ activePage, setActivePage }) => (
  <nav className="space-y-2">
    <NavItem activePage={activePage} link="#dashboard" svgIcon={<ChartPieSvg />} title="Dashboard" setActivePage={setActivePage} />
    <NavItem activePage={activePage} link="#users" svgIcon={<UsersSvg />} title="Users" setActivePage={setActivePage} />
    <NavItem activePage={activePage} link="#users" svgIcon={<ChatAltSvg />} title="Messages" setActivePage={setActivePage} />
  </nav>
);

const NavItem = ({ activePage, link, svgIcon, title, setActivePage }) => (
  <a onClick={() => setActivePage(title)} href={link} className={`flex items-center no-underline text-green-50 hover:text-green-100 p-3 rounded-md ${activePage === title ? 'bg-green-700' : ''}`}>
    {svgIcon}
    <div className="font-bold pl-3">{title}</div>
  </a>
);

const SidenavFooter = () => (
  <a href="#settings" className="flex items-center mt-auto px-3 no-underline text-green-50 opacity-70 hover:opacity-100">
    <CogSvg />
    <div className="pl-2">Settings</div>
  </a>
);

const Content = ({ activePage }) => (
  <div className="flex flex-col">
    <div className="text-xl font-bold text-gray-600 border-b-2 border-green-200 pt-6 pb-2 px-6">{activePage}</div>
    <div className="flex-1 my-6 mx-6 border-8 border-gray-200 rounded-xl border-dotted"></div>
  </div>
);

const FireSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="h-10 w-10" fill="none" viewBox="0 0 24 24" stroke="currentColor">
    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
  </svg>
);

const ChartPieSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" viewBox="0 0 20 20" fill="currentColor">
    <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z" />
    <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z" />
  </svg>
);

const UsersSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" viewBox="0 0 20 20" fill="currentColor">
    <path d="M9 6a3 3 0 11-6 0 3 3 0 016 0zM17 6a3 3 0 11-6 0 3 3 0 016 0zM12.93 17c.046-.327.07-.66.07-1a6.97 6.97 0 00-1.5-4.33A5 5 0 0119 16v1h-6.07zM6 11a5 5 0 015 5v1H1v-1a5 5 0 015-5z" />
  </svg>
);

const ChatAltSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" viewBox="0 0 20 20" fill="currentColor">
    <path d="M2 5a2 2 0 012-2h7a2 2 0 012 2v4a2 2 0 01-2 2H9l-3 3v-3H4a2 2 0 01-2-2V5z" />
    <path d="M15 7v2a4 4 0 01-4 4H9.828l-1.766 1.767c.28.149.599.233.938.233h2l3 3v-3h2a2 2 0 002-2V9a2 2 0 00-2-2h-1z" />
  </svg>
);

const CogSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
    <path fillRule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd" />
  </svg>
);


export default SideMenu;
if (document.getElementById('sideMenu')) {
    ReactDOM.render(<SideMenu />, document.getElementById('sideMenu'));
}