import React from 'react';
import ReactDOM from 'react-dom';

function HeaderAgent() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header"><a class="nav-link" href=" {{ route ('welcome')}}">Accueil </a></div>
                        <div className="card-body"></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HeaderAgent;

if (document.getElementById('headerAgent')) {
    ReactDOM.render(<HeaderAgent />, document.getElementById('headerAgent'));
}
