<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }} " style="background-image:url('img/login.jpg'); background-size: cover;>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Login - Historease</title>
</head>

<body >
    <div class="flex items-center justify-center min-h-screen">
        <div class="px-8 py-6 mt-4 text-left bg-white shadow-lg">
            <div class="flex justify-center">
                <img src="{{ asset('img/webhelpLogo.png') }}" alt="logo webhelp" width="100px" height="100px">
            </div>
            <h3 class="text-2xl font-bold text-center">Login to Historease</h3>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="mt-4">
                    <div>
                        <label class="block" for="email">{{ __('E-Mail Address') }}<label>
                                <input id="email" type="email" placeholder="Email"
                                    value="{{ old('email') }}" required autofocus name="email"
                                    class=" form-control{{ $errors->has('email') ? ' is-invalid' : '' }} w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600">
                                @if($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="mt-4">
                        <label class="block">{{ __('Password') }}<label>
                                <input id="password" type="password" placeholder="Password" name="password" required
                                    class=" form-control{{ $errors->has('password') ? ' is-invalid' : '' }} w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600">
                                @if($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="flex items-baseline justify-between">
                        <button
                            class="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900">{{ __('Login') }}</button>
                        <a href="{{ route('password.request') }}"
                            class="text-sm text-blue-600 hover:underline">Forgot password?</a>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
