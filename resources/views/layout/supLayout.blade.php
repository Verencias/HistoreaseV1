<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>@yield('title')</title>
</head>

<header>
    <nav class="bg-blue-wh shadow-lg">
        <div class="max-w  px-4 ">
            <div class="flex justify-between">
                <div class="flex space-x-7 text-white font-semibold left-0 top-0">
                    <div>
                        <!-- Website Logo -->
                        <div class="flex items-center spy-4 px-2">
                            <img src="{{ asset('img/webhelpLogo.png') }}" alt="Logo"
                                class="h-20 w-30 mr-2">
                            <h1 class="text-xl text-shadow-xl ">HISTOREASE</h1>
                        </div>
                    </div>
                    <!-- Primary Navbar items -->
                    <div class="hidden md:flex items-center space-x-1 ">
                        <a href="{{ url('accueilSup') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">Accueil</a>
                        <a href="{{ url('statsSup') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">Statistiques</a>
                        <a href="{{ url('formSup') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">
                            Nouveau formulaire</a>
                        <a href="{{ url('rechercheSup') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">Recherche</a>
                    </div>
                </div>
                {{-- Login/Profile Navbar items --}}
                <div class="hidden md:flex items-center space-x-3 text-white">
                    <p class="">Hello user {{ Auth::user()->name }}</p>
                    <img class="drop-shadow-xl" src="{{ asset('img/profile.svg') }}"
                        alt="Image de profile" width="75px" height="75px">

                </div>
                <!-- Mobile/small res menu button -->
                <div class="md:hidden flex items-center">
                    <button class="outline-none mobile-menu-button">
                        <img src="{{ asset('img/menu-burger.svg') }}" alt="burger-menu" width="24px"
                            height="24px">
                    </button>
                </div>
            </div>
        </div>
        <!-- Mobile/small menu -->
        <div class="md:hidden mobile-menu text-white">
            <ul class="">
                <li><a href="{{ url('accueilSup') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Accueil</a>
                </li>
                <li><a href="{{ url('statsSup') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Statistiques</a>
                </li>
                <li><a href="{{ url('formSup') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Nouveau
                        formulaire</a></li>
                <li><a href="{{ url('rechercheSup') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Recherche</a>
                </li>
            </ul>
        </div>
    </nav>

</header>
{{-- Side Menu --}}
<div class="flex flex-wrap bg-gray-100 w-full h-screen">
    <div class="w-fit bg-white rounded shadow-lg" href="#">
        {{-- Side Menu title --}}
        <div class="flex bg-green-wh items-center space-x-4 p-2 mb-5">
            <div>
                <button class="adm-menu-button"><img class="inline-block"
                        src="{{ asset('img/settings.svg') }}" alt="" width="24px"
                        height="24px"></button>
                <h4
                    class="adm-menu font-semibold text-lg text-gray-700 inline-block align-middle capitalize font-poppins tracking-wide">
                    Administration
                </h4>
            </div>
        </div>
        <ul class="space-y-2 text-sm">
            <li>
                <a href="{{ url('editionSup') }}"
                    class="flex items-center space-x-3 text-gray-700 p-2  font-medium hover:bg-gray-200 focus:bg-gray-200 focus:shadow-outline">
                    <span class="text-gray-600">
                        <img src="{{ asset('img/edit.svg') }}" alt="icone edition" width="20px"
                            height="20px">
                    </span>
                    <span class="adm-menu">Édition des fiches</span>
                </a>
            </li>
            <li>
                <a href="{{ url('extractionSup') }}"
                    class="flex items-center space-x-3 text-gray-700 p-2 font-medium hover:bg-gray-200 focus:bg-gray-200 focus:shadow-outline">
                    <span class="text-gray-600">
                        <img src="{{ asset('img/download.svg') }}" alt="icone extraction"
                            width="20px" height="20px">
                    </span>
                    <span class="adm-menu">Extraction des données</span>
                </a>
            </li>
            <li>
                <a href="{{ url('correctionSup') }}"
                    class="flex items-center space-x-3 text-gray-700 p-2 font-medium hover:bg-gray-200 focus:bg-gray-200 focus:shadow-outline">
                    <span class="text-gray-600">
                        <img src="{{ asset('img/pencil.svg') }}" alt="icone correction" width="20px"
                            height="20px">

                    </span>
                    <span class="adm-menu">Correction des données</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="w-9/12">
        <div class="p-4 text-gray-500">
            @yield('content')
            <footer>
                <script>
                    const btn = document.querySelector("button.mobile-menu-button");
                    const menu = document.querySelector(".mobile-menu");
                    const btnAdm = document.querySelector("button.adm-menu-button");
                    const admMenu = document.querySelectorAll(".adm-menu");
                    btn.addEventListener("click", () => {
                        menu.classList.toggle("hidden");
                    });

                    btnAdm.addEventListener("click", () => {
                        admMenu.forEach(function (menuItem) {
                            menuItem.classList.toggle('hidden');
                        });
                    });

                </script>
                C'est un footer !
            </footer>
        </div>
    </div>
</div>




</html>
