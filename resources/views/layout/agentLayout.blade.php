<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>@yield('title')</title>
</head>

<header>
    <nav class="bg-blue-wh shadow-lg">
        <div class="max-w  px-4 ">
            <div class="flex justify-between">
                <div class="flex space-x-7 text-white font-semibold left-0 top-0">
                    <div>
                        <!-- Website Logo -->
                        <div class="flex items-center spy-4 px-2">
                            <img src="{{ asset('img/webhelpLogo.png') }}" alt="Logo"
                                class="h-20 w-30 mr-2">
                            <h1 class="text-xl text-shadow-xl ">HISTOREASE</h1>
                        </div>
                    </div>
                    <!-- Primary Navbar items -->
                    <div class="hidden md:flex items-center space-x-1 ">
                        <a href="{{ url('accueilAgent') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">Accueil</a>
                        <a href="{{ url('statsAgent') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">Statistiques</a>
                        <a href="{{ url('formAgent') }}"
                            class="py-4 px-2 hover:text-red-wh transition duration-300 hover:border-b-4 border-red-wh">
                            Nouveau formulaire</a>
                    </div>
                </div>
                {{-- Login/Profile Navbar items --}}
                <div class="hidden md:flex items-center space-x-3 text-white">
                    <p class="">Hello user {{ Auth::user()->name }}</p>
                    <img class="drop-shadow-xl" src="{{ asset('img/profile.svg') }}"
                        alt="Image de profile" width="75px" height="75px">

                </div>
                <!-- Mobile/small res menu button -->
                <div class="md:hidden flex items-center">
                    <button class="outline-none mobile-menu-button">
                        <img src="{{ asset('img/menu-burger.svg') }}" alt="burger-menu" width="24px"
                            height="24px">
                    </button>
                </div>
            </div>
        </div>
        <!-- Mobile/small menu -->
        <div class="md:hidden mobile-menu text-white">
            <ul class="">
                <li><a href="{{ url('accueilAgent') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Accueil</a>
                </li>
                <li><a href="{{ url('statsAgent') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Statistiques</a>
                </li>
                <li><a href="{{ url('formAgent') }}"
                        class="block text-sm px-2 py-4 hover:bg-red-wh hover:font-semibold transition duration-300">Nouveau
                        formulaire</a></li>

            </ul>
        </div>
    </nav>

</header>

@yield('content')
<div id="sideMenu"></div>
<footer>
    <script>
        const btn = document.querySelector("button.mobile-menu-button");
        const menu = document.querySelector(".mobile-menu");
        const btnAdm = document.querySelector("button.adm-menu-button");
        const admMenu = document.querySelectorAll(".adm-menu");
        btn.addEventListener("click", () => {
            menu.classList.toggle("hidden");
        });

        btnAdm.addEventListener("click", () => {
            admMenu.forEach(function (menuItem) {
                menuItem.classList.toggle('hidden');
            });
        });

    </script>
    C'est un footer !
</footer>

</html>
