<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css')}}" rel="stylesheet" >
    <title>@yield('title')</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<header>
    <div class="bg-blue-wh text-white">
        <a href="{{ url('/accueilSup') }}">Accueil</a>
        <a href="{{ url('/statsSup') }}">  Statistiques</a>
        <a href="{{ url('/formSup') }}">   Nouveau formulaire</a>
        <p class="spacing-md">Hello user {{Auth::user()->name}}</p> 
    </div>
</header>

@yield('content')
<div id="sideMenu"></div>
<footer>
    C'est un footer !
</footer>

</html>


